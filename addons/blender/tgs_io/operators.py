from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty, FloatProperty
from bpy.types import Operator

from . import blender_export


class TgsExporterBase(Operator):
    def execute(self, context):
        animation = blender_export.context_to_tgs(context)
        self._export_animation(animation)
        return {'FINISHED'}

    def _export_animation(self, animation):
        raise NotImplementedError()


class TgsExporterTgs(TgsExporterBase, ExportHelper):
    """
    Export Telegram animated sticker
    """
    format = "tgs"
    bl_label = "Telegram Sticker (*.tgs)"
    bl_idname = "tgs.export_" + format

    filename_ext = "." + format

    filter_glob: StringProperty(
        default="*." + format,
        options={'HIDDEN'},
        maxlen=255,
    )

    def _export_animation(self, animation):
        blender_export.tgs.exporters.export_tgs(animation, self.filepath)


class TgsExporterLottie(TgsExporterBase, ExportHelper):
    """
    Export Lottie JSON
    """
    format = "json"
    bl_label = "Lottie (.json)"
    bl_idname = "tgs.export_" + format

    filename_ext = "." + format

    filter_glob: StringProperty(
        default="*." + format,
        options={'HIDDEN'},
        maxlen=255,
    )

    pretty: BoolProperty(
        name="Pretty",
        description="Pretty print the resulting JSON",
        default=False,
    )

    def _export_animation(self, animation):
        blender_export.tgs.exporters.export_lottie(animation, self.filepath, self.pretty)


class TgsExporterHtml(TgsExporterBase, ExportHelper):
    """
    Export HTML with an embedded Lottie viewer
    """
    format = "html"
    bl_label = "Lottie HTML (.html)"
    bl_idname = "tgs.export_" + format

    filename_ext = "." + format

    filter_glob: StringProperty(
        default="*." + format,
        options={'HIDDEN'},
        maxlen=255,
    )

    def _export_animation(self, animation):
        blender_export.tgs.exporters.export_embedded_html(animation, self.filepath)


registered_classes = [TgsExporterTgs, TgsExporterLottie, TgsExporterHtml]
